#pragma once 
 
#include "types.h"

template<
    typename a1 = u64,
    typename a2 = u64,
    typename a3 = u64,
    typename a4 = u64,
    typename a5 = u64,
    typename a6 = u64,
    typename a7 = u64,
    typename a8 = u64,
    typename a9 = u64,
    typename a10 = u64,
    typename a11 = u64,
    typename a12 = u64,
    typename a13 = u64,
    typename a14 = u64,
    typename a15 = u64
>
struct Arguments {
    a1 a = 0;
    a2 b = 0;
    a3 c = 0;
    a4 d = 0;
    a5 e = 0;

    a6 f = 0;
    a7 g = 0;
    a8 h = 0;
    a9 i = 0;
    a1o j = 0;

    a11 k = 0;
    a12 l = 0;
    a13 m = 0;
    a14 n = 0;
    a15 o = 0;
};