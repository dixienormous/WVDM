#pragma once


#include "WVDM.hpp"
#include "types.h"

class VMPA {
	u64 usermode_pml4_pa = 0;
	WVDM& wvdm;
	bool init_success;
public:
	VMPA(WVDM& wvdm_, u64 um_pml4_pa_);
	u64 vm_to_pa(u64 vm_);
	void readvm(u64 vm_, void* outBuffer_, int size_);
	void writevm(u64 vm_, void* outBuffer_, int size_);

    void test();
};




//PAGING STRUCTS
typedef union _virt_addr_t
{
    u64 value;
    struct
    {
        u64 offset : 12;
        u64 pt_index : 9;
        u64 pd_index : 9;
        u64 pdpt_index : 9;
        u64 pml4_index : 9;
        u64 reserved : 16;
    };
} virt_addr_t, * pvirt_addr_t;

typedef union _pml4e
{
    u64 value;
    struct
    {
        u64 present : 1;           // Must be 1, region invalid if 0.
        u64 rw : 1;                 // If 0, writes not allowed.
        u64 user_supervisor : 1;   // If 0, user-mode accesses not allowed.
        u64 page_write_through : 1; // Determines the memory type used to access PDPT.
        u64 page_cache : 1;        // Determines the memory type used to access PDPT.
        u64 accessed : 1;          // If 0, this entry has not been used for translation.
        u64 Ignored1 : 1;
        u64 page_size : 1;         // Must be 0 for PML4E.
        u64 Ignored2 : 4;
        u64 pfn : 36;              // The page frame number of the PDPT of this PML4E.
        u64 reserved : 4;
        u64 Ignored3 : 11;
        u64 nx : 1;                // If 1, instruction fetches not allowed.
    };
} pml4e_t, * ppml4e_t;

typedef union _pdpte
{
    u64 value;
    struct
    {
        u64 present : 1;            // Must be 1, region invalid if 0.
        u64 rw : 1;                 // If 0, writes not allowed.
        u64 user_supervisor : 1;    // If 0, user-mode accesses not allowed.
        u64 page_write : 1;          // Determines the memory type used to access PD.
        u64 page_cache : 1;         // Determines the memory type used to access PD.
        u64 accessed : 1;           // If 0, this entry has not been used for translation.
        u64 Ignored1 : 1;
        u64 page_size : 1;          // If 1, this entry maps a 1GB page.
        u64 Ignored2 : 4;
        u64 pfn : 36;               // The page frame number of the PD of this PDPTE.
        u64 reserved : 4;
        u64 Ignored3 : 11;
        u64 nx : 1;                 // If 1, instruction fetches not allowed.
    };
} pdpte_t, * ppdpte_t;

typedef union _pde
{
    u64 value;
    struct
    {
        u64 present : 1;            // Must be 1, region invalid if 0.
        u64 rw : 1;                 // If 0, writes not allowed.
        u64 user_supervisor : 1;    // If 0, user-mode accesses not allowed.
        u64 page_write : 1;          // Determines the memory type used to access PD.
        u64 page_cache : 1;         // Determines the memory type used to access PD.
        u64 accessed : 1;           // If 0, this entry has not been used for translation.
        u64 Ignored1 : 1;
        u64 page_size : 1;          // If 1, this entry maps a 1GB page.
        u64 Ignored2 : 4;
        u64 pfn : 36;               // The page frame number of the PD of this PDPTE.
        u64 reserved : 4;
        u64 Ignored3 : 11;
        u64 nx : 1;                 // If 1, instruction fetches not allowed.
    };
} pde_t, * ppde_t;

typedef union _pte
{
    u64 value;
    struct
    {
        u64 present : 1;          // Must be 1, region invalid if 0.
        u64 rw : 1;               // If 0, writes not allowed.
        u64 user_supervisor : 1;  // If 0, user-mode accesses not allowed.
        u64 page_write : 1;        // Determines the memory type used to access the memory.
        u64 page_cache : 1;       // Determines the memory type used to access the memory.
        u64 accessed : 1;         // If 0, this entry has not been used for translation.
        u64 dirty : 1;             // If 0, the memory backing this page has not been written to.
        u64 page_access_type : 1;  // Determines the memory type used to access the memory.
        u64 global : 1;            // If 1 and the PGE bit of CR4 is set, translations are global.
        u64 ignored2 : 3;
        u64 pfn : 36;             // The page frame number of the backing physical page.
        u64 reserved : 4;
        u64 ignored3 : 7;
        u64 protect_key : 4;       // If the PKE bit of CR4 is set, determines the protection key.
        u64 nx : 1;               // If 1, instruction fetches not allowed.
    };
} pte_t, * ppte_t;